**Créer un site pour afficher l'agenda des actualités de votre ville**

vite > npm create vite@latest

Une actualité est composée de :
- Un identifiant
- Un titre
- Une description
- Le nom du lieu
- Une date de début
- Une date de fin
- Un tarif

Toutes les pages afficheront une entête avec le nom du site et un menu comprenant les options "Accueil", "Agenda" et "Nous contacter".

La page d'accueil présentera un titre "Nos actualités", un texte d'introduction (utilisant du lorem ipsum) et une image.

L'agenda affichera la liste des actualités (basée sur un tableau d'actualités "en dur").

Dans un premier temps, l'accueil et l'agenda seront affichés sur la même page.

Il sera possible de sélectionner une actualité pour indiquer son intérêt à y participer (exemples : "Je participe", "Annuler ma participation").

Améliorez le système pour charger les actualités depuis une API : [API des actualités de Toulouse Métropole](https://data.toulouse-metropole.fr/api/explore/v2.1/catalog/datasets/agenda-des-manifestations-culturelles-so-toulouse/records?limit=20).

Intégrez un routeur pour faciliter la navigation entre les pages.

Créez un layout avec un header et utilisez des Outlet pour l'organisation du contenu.

Stockez les informations de participation dans le localStorage.

Ajoutez une page "Mes participations" affichant la liste des actualités ajoutées au localStorage.