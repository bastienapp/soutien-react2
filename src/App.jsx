import "./App.css";
import Navigation from "./components/Navigation"
import Title from "./components/Title"

// avant c'était une classe
function App() {
  // JSX : une façon de faire de l'html avec JavaScript
  // Navigation is not defined
  return (
    <>
      <Navigation />
      <Title content="Toulouse News" colour="red" />
      <Title content="Montpellier News" colour="blue" />

    </>
  );
}

export default App;
