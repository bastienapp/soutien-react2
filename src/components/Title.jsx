/*
composant Title pour afficher un titre h1
le texte pourra changer lors de l'appel du composant
*/
function Title (props) {
  /*
  props c'est un objet qui contient ce qu'on a envoyé par le parent
  <Title content="Tacos" colour="blue" />

  props : {
    content: "Tacos",
    colour: "blue"
  }
  */
    // {"content":"Toulouse News"}
    return (
        <h1 style={{color: props.colour}}>{props.content}</h1>
    )
}

export default Title;