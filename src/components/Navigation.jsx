function Navigation() {
  return (
    <nav>
      <ul>
        <li>Accueil</li>
        <li>Agenda</li>
        <li>Nous contacter</li>
      </ul>
    </nav>
  );
}

export default Navigation;
